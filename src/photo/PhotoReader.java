package photo;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.drew.imaging.ImageProcessingException;
import com.sun.org.apache.xpath.internal.SourceTree;
import util.DateFormatter;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import java.nio.file.Files;


import static java.lang.System.exit;

public class PhotoReader 
{
	static Logger log = LogManager.getLogger(PhotoReader.class);

	public static void main(String[] args) {

		String input="";
		String output="";
		Boolean addOriginalFilename = false;

		if (args.length <2 )
		{
			System.out.println("Nie podales sciezki in i out");
			exit(1);
		}
		else {
			input = args[0];
			output = args[1];
		}

		if (args.length == 3 && args[2].trim().equalsIgnoreCase("y"))
			addOriginalFilename =true;

		try{
			log.trace("Entering app..");
			final File dir =  new File(input);
			for(File f: dir.listFiles())
			{

				if(f.isFile())
				{
					Metadata meta =null;
					ExifSubIFDDirectory dirss;
					Date d;
					ExifIFD0Directory directory;
					try{
						 meta = ImageMetadataReader.readMetadata(f);
						 if(meta == null)
						{
							System.out.println("metadane nullowe");
						}
						System.out.println("nie null");
					dirss = meta.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
						System.out.println("tu ok");
					d = dirss.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
					directory = meta.getFirstDirectoryOfType(ExifIFD0Directory.class);

					}
					catch(ImageProcessingException e){
						System.out.println("Nieobslugiwany format pliku: "+ f.getAbsoluteFile());
						continue;
					}
					catch(Exception e){
						System.out.println("Brak metadanych: " + f.getAbsoluteFile());
						continue;
					}
					String data = DateFormatter.format(d);
					data = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(d);
					String extension = "";

					int i = f.getName().lastIndexOf('.');
					if (i > 0) {
						extension = f.getName().substring(i+1);
					}

					for(Tag tag: directory.getTags())
					{
						if(tag.getTagName().equals("Model"))
						{
						
							try{
								Path out;
								if (addOriginalFilename)
									out = Paths.get(output,data+"_"+tag.getDescription()+"_"+f.getName());//+"."+extension);
								else
									out =Paths.get(output,data+"_"+tag.getDescription()+"."+extension);
								Files.copy(f.toPath(),out, StandardCopyOption.REPLACE_EXISTING);
								System.out.println("Skopiowano plik: "+f.getAbsolutePath() +" do: "+out.toString());

							}
							catch(Exception e)
							{
								e.printStackTrace();
							}
							
							
							break;
						}
					}
				
					}


					
				}
				
			


		}

		catch(Exception e)
		{e.printStackTrace();}
	}

}
