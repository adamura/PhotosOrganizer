package util;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


public class DateFormatter {

	public static String format(Date d)
	{
		Calendar cal = new GregorianCalendar();
		cal.setTime(d);
		TimeZone tz = TimeZone.getTimeZone("UTC+02:00");
		cal.setTimeZone(tz);
		StringBuilder data = new StringBuilder();
		data.append(cal.get(Calendar.YEAR));
		data.append(" ");
		//Locale loc = Locale.ENGLISH;
		//data.append(cal.getDisplayName(Calendar.MONTH, Calendar.SHORT_FORMAT,loc));
		data.append(cal.get(Calendar.MONTH)+1);
		data.append(" ");
		data.append(cal.get(Calendar.DAY_OF_MONTH));
		data.append(" ");
		data.append(cal.get(Calendar.HOUR_OF_DAY));
		data.append("_");
		data.append(cal.get(Calendar.MINUTE));
		data.append("_");
		data.append(cal.get(Calendar.SECOND));
		return data.toString();
	}
}
